from __future__ import print_function
from __future__ import division
from pykml import parser
import argparse
import logging
import hashlib
import urllib
import time
import math
import os

module_logger = logging.getLogger('HyperlapseGenerator')

class GoogleImageCollector:

    output_folder = ""
    kml_file = ""
    api_key = ""
    included_extensions = ['jpg']

    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Filters and downloads list of images from google image api")
        self.group = self.parser.add_mutually_exclusive_group()
        self.group.add_argument("-v", "--verbose", action="store_true", dest='verbose')
        self.group.add_argument("-q", "--quiet", action="store_true")
        self.parser.add_argument("-i", "--input", type=str, dest='kml', help="KML Route in LineString format")
        self.parser.add_argument("-o", "--output", type=str, dest='out_folder', help="Folder for output images")
        self.parser.add_argument("-k", "--key", type=str, dest='key', help="Google Image API Key")
        self.args = self.parser.parse_args()

        # Logging configuration
        logging.basicConfig(filename="streetmapper.log", level=logging.DEBUG)
        self.logger = logging.getLogger('HyperlapseGenerator')
        self.handler = logging.StreamHandler()
        if self.args.verbose:
            self.logger.setLevel(logging.DEBUG)
            print("Debug Logging Level")
        elif self.args.quiet:
            self.logger.setLevel(logging.CRITICAL)
            print("Critical Logging Level")
        else:
            self.logger.setLevel(logging.INFO)
            print("Information Logging Level")
        self.formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        if self.args.kml:
            self.kml_file = self.args.kml
            module_logger.debug("KML: " + self.kml_file)
        else:
            try:
                raise Exception("kml_file must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.out_folder:
            self.output_folder = self.args.out_folder
            module_logger.debug("Output folder: " + self.output_folder)
        else:
            try:
                raise Exception("kml_file must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.key:
            self.api_key = "&key=" + self.args.key
            module_logger.debug("API key: " + self.api_key)
        else:
            try:
                raise Exception("API Key must be provided")
            except Exception as e:
                module_logger.error(e)

    module_logger.info("Creating folders")

    # Create the folders if they don't exist
    def _create_output_folders(self):
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)
        # if not os.path.exists(os.path.join(self.output_folder, 'N')):
        #     os.makedirs(os.path.join(self.output_folder, 'N'))
        # if not os.path.exists(os.path.join(self.output_folder, 'NE')):
        #     os.makedirs(os.path.join(self.output_folder, 'NE'))
        # if not os.path.exists(os.path.join(self.output_folder, 'E')):
        #     os.makedirs(os.path.join(self.output_folder, 'E'))
        # if not os.path.exists(os.path.join(self.output_folder, 'SE')):
        #     os.makedirs(os.path.join(self.output_folder, 'SE'))
        # if not os.path.exists(os.path.join(self.output_folder, 'S')):
        #     os.makedirs(os.path.join(self.output_folder, 'S'))
        # if not os.path.exists(os.path.join(self.output_folder, 'W')):
        #     os.makedirs(os.path.join(self.output_folder, 'W'))
        # if not os.path.exists(os.path.join(self.output_folder, 'NW')):
        #     os.makedirs(os.path.join(self.output_folder, 'NW'))
        # if not os.path.exists(os.path.join(self.output_folder, 'SW')):
        #     os.makedirs(os.path.join(self.output_folder, 'SW'))
        if not os.path.exists(os.path.join(self.output_folder, 'forward')):
            os.makedirs(os.path.join(self.output_folder, 'forward'))

    # Build the url for street image, send to google, save to disk
    def _get_street(self, geo_loc, bearing, output_image_name):
        self._create_output_folders()
        output_folder = self.output_folder

        base_url = "https://maps.googleapis.com/maps/api/streetview?size=640x640&fov=120&pitch=20&&location="
        heading_n = (r'&heading=0', os.path.join(output_folder, 'forward', output_image_name + 'N' + ".jpg"))
        heading_nne = (r'&heading=23', os.path.join(output_folder, 'forward', output_image_name + 'NNE' + ".jpg"))
        heading_ne = (r'&heading=45', os.path.join(output_folder, 'forward', output_image_name + 'NE' + ".jpg"))
        heading_ene = (r'&heading=68', os.path.join(output_folder, 'forward', output_image_name + 'ENE' + ".jpg"))
        heading_e = (r'&heading=90', os.path.join(output_folder, 'forward', output_image_name + 'E' + ".jpg"))
        heading_ese = (r'&heading=113', os.path.join(output_folder, 'forward', output_image_name + 'ESE' + ".jpg"))
        heading_se = (r'&heading=135', os.path.join(output_folder, 'forward', output_image_name + 'SE' + ".jpg"))
        heading_sse = (r'&heading=158', os.path.join(output_folder, 'forward', output_image_name + 'SSE' + ".jpg"))
        heading_s = (r'&heading=180', os.path.join(output_folder, 'forward', output_image_name + 'S' + ".jpg"))
        heading_ssw = (r'&heading=203', os.path.join(output_folder, 'forward', output_image_name + 'SSW' + ".jpg"))
        heading_sw = (r'&heading=225', os.path.join(output_folder, 'forward', output_image_name + 'SW' + ".jpg"))
        heading_wsw = (r'&heading=258', os.path.join(output_folder, 'forward', output_image_name + 'WSW' + ".jpg"))
        heading_w = (r'&heading=270', os.path.join(output_folder, 'forward', output_image_name + 'W' + ".jpg"))
        heading_wnw = (r'&heading=293', os.path.join(output_folder, 'forward', output_image_name + 'WNW' + ".jpg"))
        heading_nw = (r'&heading=315', os.path.join(output_folder, 'forward', output_image_name + 'NW' + ".jpg"))
        heading_nnw = (r'&heading=338', os.path.join(output_folder, 'forward', output_image_name + 'NNW' + ".jpg"))

        street_url = base_url + geo_loc + self.api_key

        image_url_dict = {
            "N": (street_url + heading_n[0], heading_n[1]),
            "NNE": (street_url + heading_nne[0], heading_nne[1]),
            "NE": (street_url + heading_ne[0], heading_ne[1]),
            "ENE": (street_url + heading_ene[0], heading_ene[1]),
            "E": (street_url + heading_e[0], heading_e[1]),
            "ESE": (street_url + heading_ese[0], heading_ese[1]),
            "SE": (street_url + heading_se[0], heading_se[1]),
            "SSE": (street_url + heading_sse[0], heading_sse[1]),
            "S": (street_url + heading_s[0], heading_s[1]),
            "SSW": (street_url + heading_ssw[0], heading_ssw[1]),
            "SW": (street_url + heading_sw[0], heading_sw[1]),
            "WSW": (street_url + heading_wsw[0], heading_wsw[1]),
            "W": (street_url + heading_w[0], heading_w[1]),
            "WNW": (street_url + heading_wnw[0], heading_wnw[1]),
            "NW": (street_url + heading_nw[0], heading_nw[1]),
            "NNW": (street_url + heading_nnw[0], heading_nnw[1])
        }

        if not os.path.isfile(image_url_dict[bearing][1]):
            module_logger.info("download: " + image_url_dict[bearing][0])
            module_logger.info("Saving " + image_url_dict[bearing][1])
            urllib.urlretrieve(image_url_dict[bearing][0], image_url_dict[bearing][1])
        else:
            module_logger.info("File already exists:" + image_url_dict[bearing][1])

    # Parse the LineString in the kml file for coordinates, swaps them for the google street api
    # and passes them to get_street() function
    def route_download(self):
        kml_root = parser.fromstring(open(self.kml_file, 'r').read())
        locations = str.split(str(kml_root.Document.Placemark.LineString.coordinates), ",0.0")
        location_list, bearing_list = self.get_locations_with_direction()

        i = 0
        for location, bearing in zip(location_list, bearing_list):
            module_logger.debug(str(location) + " : " + str(bearing))
            i += 1
            point = str.split(location, ",")

            if 0 < i < 10:
                self._get_street(geo_loc=str(point[1] + "," + point[0]), bearing=bearing,
                                 output_image_name="000" + str(i))
            elif 10 <= i < 100:
                self._get_street(geo_loc=str(point[1] + "," + point[0]), bearing=bearing,
                                 output_image_name="00" + str(i))
            elif 100 <= i < 1000:
                self._get_street(geo_loc=str(point[1] + "," + point[0]), bearing=bearing,
                                 output_image_name="0" + str(i))
            elif 1000 <= i < 10000:
                self._get_street(geo_loc=str(point[1] + "," + point[0]), bearing=bearing,
                                 output_image_name=str(i))

    def get_locations_with_direction(self):
        root = parser.fromstring(open(self.kml_file, 'r').read())
        locations = str.split(str(root.Document.Placemark.LineString.coordinates), ",0.0")

        #Place the locations into a list and filter out any empty elements
        location_list = []
        for location in locations:
            if not location:
                continue
            location_list.append(location)

        #get the final length of the GPS locations
        list_length = len(location_list)

        # Scan over the location list and figure out the direction of travel
        # for each entry from list[0] to list[N-2] (-2 because of zero index)
        bearing_list = []
        for index in range(list_length - 2):

            #Split the string for the locations
            current_positon = str.split(location_list[index], ",")
            next_position = str.split(location_list[index+1], ",")

            degrees = self._calc_direction_simple(current_positon,next_position)
            if not degrees:
                print ("here!")

            bearing_list.append(self._convert_to_compass_direction(degrees))

        return location_list, bearing_list

    @staticmethod
    def _get_hash(file_to_hash):
        hasher = hashlib.sha256()
        with open(file_to_hash, "rb") as f:
            byte = f.read(1)
            while byte != "":
                byte = f.read(1)
                hasher.update(byte)
        f.close()

        return hasher.hexdigest()

    # Per folder and for each image file, compares with other images in the folder who's name contains a number
    # which is within plus or minus 6 of it's own image number
    def delete_duplicate_images(self, target_dir):
        start = time.time()

        file_names = [fn for fn in sorted(os.listdir(target_dir))
                      if any(fn.endswith(ext) for ext in self.included_extensions)]
        hashed_list = list()

        for file_name in file_names:
            image_location = os.path.join(target_dir, file_name)
            h = self._get_hash(file_to_hash=image_location)
            module_logger.debug(file_name + " : " + h)
            hashed_list.append(h)

        result = {}
        dup = {}
        for file_name, value in zip(file_names, hashed_list):
            if value not in result.values():
                result[file_name] = value
            else:
                dup[file_name] = file_name

        for file_name in dup:
            module_logger.info("Deleting: " + os.path.join(target_dir, file_name))
            os.remove(os.path.join(target_dir, file_name))

        end = time.time()
        module_logger.debug("Time taken: " + str(end - start))
        module_logger.debug("Number of duplicates:" + str(len(dup)))

        # ref: https://pypi.python.org/pypi/utm
        # utm: Universal Tranverse Metacor
        # represents a projection (with ( hopefully acceptable ) errors due to
        # nature of the conversion) of lat/lon onto a zone in a grid.

    @staticmethod
    def _calc_direction_simple(current_position, next_position):

        startLat = math.radians(float(current_position[0]))
        startLong = math.radians(float(current_position[1]))
        endLat = math.radians(float(next_position[0]))
        endLong = math.radians(float(next_position[1]))

        dLong = endLong - startLong

        dPhi = math.log(math.tan(endLat / 2.0 + math.pi / 4.0) / math.tan(startLat / 2.0 + math.pi / 4.0))
        if abs(dLong) > math.pi:
            if dLong > 0.0:
                dLong = -(2.0 * math.pi - dLong)
            else:
                dLong = (2.0 * math.pi + dLong)

        bearing = (math.degrees(math.atan2(dLong, dPhi)) + 360.0) % 360.0;

        return bearing

    @staticmethod
    def _convert_to_compass_direction(degrees):
        #Catch when degrees is None and ensure that it is a float
        if (
                    degrees is None or not isinstance(degrees,float)
        ):
            try:
                Exception("Invalid angle")
            except Exception as e:
                module_logger.error(e)
            return "Invalid angle"

        # convert to integer
        degrees = int(degrees)

        # print(degrees)
        if degrees in range(0, 11):
            module_logger.debug("E")
            return 'E'
        elif degrees in range(11, 33):
            module_logger.debug("ENE")
            return 'ENE'
        elif degrees in range(33, 55):
            module_logger.debug("NE")
            return 'NE'
        elif degrees in range(55, 77):
            module_logger.debug("NNE")
            return 'NNE'
        elif degrees in range(77, 99):
            module_logger.debug("N")
            return 'N'
        elif degrees in range(99, 121):
            module_logger.debug("NNW")
            return 'NNW'
        elif degrees in range(121, 143):
            module_logger.debug("NW")
            return 'NW'
        elif degrees in range(143, 165):
            module_logger.debug("WNW")
            return 'WNW'
        elif degrees in range(165, 187):
            module_logger.debug("W")
            return 'W'
        elif degrees in range(187, 209):
            module_logger.debug("WSW")
            return 'WSW'
        elif degrees in range(209, 231):
            module_logger.debug("SW")
            return 'SW'
        elif degrees in range(231, 253):
            module_logger.debug("SSW")
            return 'SSW'
        elif degrees in range(253, 275):
            module_logger.debug("S")
            return 'S'
        elif degrees in range(275, 297):
            module_logger.debug("SSE")
            return 'SSE'
        elif degrees in range(297, 319):
            module_logger.debug("SE")
            return 'SE'
        elif degrees in range(319, 341):
            module_logger.debug("ESE")
            return 'ESE'
        elif degrees in range(341, 360):
            module_logger.debug("E")
            return 'E'
        else:
            try:
                print("Angle outside 0-360 (" + str(degrees) + ")")
                raise Exception("Angle outside 0-360")
            except Exception as e:
                module_logger.error(e)


def main():
    gic = GoogleImageCollector()
    gic.route_download()
    # gic.delete_duplicate_images(os.path.join(gic.output_folder, "forward"))


if __name__ == "__main__":
    main()

