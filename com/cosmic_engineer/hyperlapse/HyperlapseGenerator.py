from __future__ import division
from __future__ import print_function

import argparse
import logging
import os
import shutil

import cv2

import GoogleImageCollector
import ImageProcessor

# create logger
module_logger = logging.getLogger('HyperlapseGenerator')

# TODO 1. Stitch images together into pano - http://www.pyimagesearch.com/2016/01/11/opencv-panorama-stitching/ - On Hold
# TODO 4. Enable API billing: 2k x 2k images
# TODO 6. Check for empty images in the blend
# TODO 7. Check for proper kml file
# TODO 8. Implement some resumable functionality
# TODO 9. Perform the blending in a temporary folder - Done
# TODO 10. Enable command line options for everything - Done (until more needed)
# TODO 11. Implement a command line option for an action - blend only or blend and video or only video - Done
# TODO 12. Cleanup temporary workspace - Done


class HyperlapseGenerator(ImageProcessor.ImageProcessor,
                          GoogleImageCollector.GoogleImageCollector):
    output_folder = ""
    kml_file = ""
    api_key = ""
    included_extensions = ['jpg']
    blend_steps = "10"
    video_fps = "120"
    workspace_folder = ".hyperlapse" #Place it in the execution directory
    operation_phase = "0"
    cleanup_workspace = "No"

    def __init__(self):
        self.parser = argparse.ArgumentParser(description="Generates hyperlapse video from KML route")
        self.group = self.parser.add_mutually_exclusive_group()
        self.group.add_argument("-v", "--verbose", action="store_true", dest='verbose')
        self.group.add_argument("-q", "--quiet", action="store_true")
        self.parser.add_argument("-i", "--input", type=str, dest='kml', help="KML Route in LineString format")
        self.parser.add_argument("-o", "--output", type=str, dest='out_folder', help="Folder for output images")
        self.parser.add_argument("-k", "--key", type=str, dest='key', help="Google Image API Key")
        self.parser.add_argument("-b", "--blend", type=str, dest='blend_steps', help="Number of steps for blending images")
        self.parser.add_argument("-f", "--fps", type=str, dest='video_fps', help="Frames per second for the generated video")
        self.parser.add_argument("-w", "--workspace", type=str, dest='workspace_folder', help="Temporary folder to store the blended images")
        self.parser.add_argument("-p", "--phase", type=str, dest='operation_phase',
                                 help="1: download images + remove duplicates, 2: blend images 3: generate video, 4: download + remove + blend, 5: download + remove + blend + video, 6: blend + video" )
        self.parser.add_argument("-c", "--cleanup", type=str, dest='cleanup_workspace',
                                 help="Cleanup the workspace folder, Yes or No")

        self.args = self.parser.parse_args()

        # Logging configuration
        logging.basicConfig(filename="streetmapper.log", level=logging.DEBUG)
        self.logger = logging.getLogger('HyperlapseGenerator')
        self.handler = logging.StreamHandler()
        if self.args.verbose:
            self.logger.setLevel(logging.DEBUG)
            print("Debug Logging Level")
        elif self.args.quiet:
            self.logger.setLevel(logging.CRITICAL)
            print("Critical Logging Level")
        else:
            self.logger.setLevel(logging.INFO)
            print("Information Logging Level")
        self.formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        if self.args.kml:
            self.kml_file = self.args.kml
            module_logger.debug("KML: " + self.kml_file)
        else:
            try:
                raise Exception("kml_file must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.out_folder:
            self.output_folder = self.args.out_folder
            module_logger.debug("Output folder: " + self.output_folder)
        else:
            try:
                raise Exception("Output folder must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.key:
            self.api_key = "&key=" + self.args.key
            module_logger.debug("API key: " + self.api_key)
        else:
            try:
                raise Exception("API Key must be provided")
            except Exception as e:
                module_logger.error(e)
        if self.args.operation_phase:
            self.operation_phase = self.args.operation_phase
        module_logger.debug("Operation Phase: " + self.operation_phase)
        if self.args.video_fps:
            self.video_fps = self.args.video_fps
        module_logger.debug("Video FPS set to : " + self.video_fps)
        if self.args.blend_steps:
            self.blend_steps = self.args.blend_steps
        module_logger.debug("Blend Steps: " + self.blend_steps)
        if self.args.workspace_folder:
            self.workspace_folder = self.args.workspace_folder
        module_logger.debug("Workspace Folder: " + self.workspace_folder)
        if self.args.cleanup_workspace:
            self.cleanup_workspace = self.args.cleanup_workspace
        module_logger.debug("Cleaning Up Workspace Folder: " + self.cleanup_workspace)

        # Detect OpenCV version
        self.isv3 = cv2.__version__
        self.logger.info("OpenCV Version: " + self.isv3)

    # define the function blocks
    def operation_zero(self):
        print ("No operation to execute, exiting")

    def operation_one(self):
        module_logger.debug("Beginning Route Downloading and deleting duplicates")
        self.route_download()
        self.delete_duplicate_images(os.path.join(self.output_folder, "forward"))
        module_logger.debug("Finishing Route Downloading and deleting duplicates")

    def operation_two(self):
        module_logger.debug("Beginning Blending")
        self.blend_folder_images(os.path.join(self.output_folder, "forward"),self.workspace_folder,int(self.blend_steps))
        module_logger.debug("Finishing Blending")

    def operation_three(self):
        module_logger.debug("Beginning Video Generation")
        self.generage_blended_movie_ffmgp(self.workspace_folder, "out.mp4",
                                          self.video_fps)
        module_logger.debug("Finishing Video Generation")

    def operation_four(self):
        self.operation_one()
        self.operation_two()

    def operation_five(self):
        self.operation_one()
        self.operation_two()
        self.operation_three()

    def operation_six(self):
        self.operation_two()
        self.operation_three()

    # map the inputs to the function blocks
    operation_phases = {
       0: operation_zero,
       1 : operation_one,
       2 : operation_two,
       3 : operation_three,
       4 : operation_four,
       5 : operation_five,
       6 : operation_six
    }

    def clean_workspace(self):
        if(os.path.exists(self.workspace_folder)):
            shutil.rmtree(self.workspace_folder)
            module_logger.debug("Removed Workspace Directory: " + self.workspace_folder)

def main():
    hg = HyperlapseGenerator()

    # Perform the execution phase
    module_logger.debug("Executing phase " + hg.operation_phase)
    hg.operation_phases[int(hg.operation_phase)](hg)

    if(hg.cleanup_workspace.lower() == "yes"):
        hg.clean_workspace()


if __name__ == "__main__":
    main()
