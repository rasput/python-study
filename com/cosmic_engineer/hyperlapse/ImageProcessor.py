from __future__ import print_function
from __future__ import division
from PIL import ImageDraw
from PIL import Image
from ffmpy import FFmpeg
import numpy as np
from subprocess import call
import argparse
import logging
import imutils
import shutil
import cv2
import os

module_logger = logging.getLogger('ImageProcessor')


class ImageProcessor:

    output_folder = ""
    kml_file = ""
    api_key = ""
    included_extensions = ['jpg']

    isv3 = imutils.is_cv3()
    
    def __init__(self):
        self.parser = argparse.ArgumentParser(description="")
        self.group = self.parser.add_mutually_exclusive_group()
        self.group.add_argument("-v", "--verbose", action="store_true", dest='verbose')
        self.group.add_argument("-q", "--quiet", action="store_true")
        self.parser.add_argument("-i", "--input", type=str, dest='kml', help="KML Route in LineString format")
        self.parser.add_argument("-o", "--output", type=str, dest='out_folder', help="Folder for output images")
        self.parser.add_argument("-k", "--key", type=str, dest='key', help="Google Image API Key")
        self.args = self.parser.parse_args()

        # Logging configuration
        logging.basicConfig(filename="streetmapper.log", level=logging.DEBUG)
        self.logger = logging.getLogger('HyperlapseGenerator')
        self.handler = logging.StreamHandler()
        if self.args.verbose:
            self.logger.setLevel(logging.DEBUG)
            print("Debug Logging Level")
        elif self.args.quiet:
            self.logger.setLevel(logging.CRITICAL)
            print("Critical Logging Level")
        else:
            self.logger.setLevel(logging.INFO)
            print("Information Logging Level")
        self.formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler)

        if self.args.kml:
            self.kml_file = self.args.kml
            module_logger.debug("KML: " + self.kml_file)
        else:
            try:
                raise Exception("kml_file must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.out_folder:
            self.output_folder = self.args.out_folder
            module_logger.debug("Output folder: " + self.output_folder)
        else:
            try:
                raise Exception("kml_file must be specified")
            except Exception as e:
                module_logger.error(e)
        if self.args.key:
            self.api_key = "&key=" + self.args.key
            module_logger.debug("API key: " + self.api_key)
        else:
            try:
                raise Exception("API Key must be provided")
            except Exception as e:
                module_logger.error(e)

        # Detect OpenCV version
        # self.isv3 = cv2.__version__
        self.logger.info("OpenCV Version: " + str(self.isv3))

    def generage_blended_movie_ffmgp(self, source_dir, output_file, video_fps):
        if not os.path.exists(source_dir):
            module_logger.debug("The source directory " + source_dir + " does not exist, returning")
            return

        FFMPEG_BIN = "ffmpeg" # For Linux + Mac

        # FFMPEG command reference https://trac.ffmpeg.org/wiki/Create%20a%20video%20slideshow%20from%20images
        raw_data_folder = "'" + os.path.join(source_dir, '*.jpg') + "'"

        system_command = FFMPEG_BIN + " -y -framerate " + video_fps + " -pattern_type glob -i " + raw_data_folder + " -c:v libx264 " + output_file

        module_logger.debug("Generating video with command:  " + system_command)

        os.system(system_command)

    def blend_folder_images(self, source_dir, workspace_dir, blend_steps):
        file_names = [fn for fn in sorted(os.listdir(source_dir))
                      if any(fn.endswith(ext) for ext in self.included_extensions)]

        # Ensure that the working directory is there and cleared
        if os.path.exists(workspace_dir):
            shutil.rmtree(workspace_dir)

        os.makedirs(workspace_dir)

        alpha = 0.0
        image_location = ""
        number_of_files = len(file_names)
        for file_index in range(0,number_of_files-1):

            first_image = file_names[file_index]
            first_image_path = os.path.join(source_dir,first_image)
            second_image = file_names[file_index+1]
            second_image_path = os.path.join(source_dir, second_image)

            raw_image = Image.open(first_image_path)
            workspace_key_image_location = os.path.join(workspace_dir, first_image)

            if os.path.isfile(workspace_key_image_location):
                module_logger.info(workspace_key_image_location + " file already exist, skipping")
            else:
                module_logger.info(workspace_key_image_location)
                raw_image.save(workspace_key_image_location)

            _alpha = alpha
            for steps in xrange(blend_steps, 0, -1):
                module_logger.debug("pre:  " + first_image)
                module_logger.debug("curr: " + second_image)
                _image, _alpha = self._blend(second_image_path, first_image_path, _alpha,
                                             float(1 / (blend_steps+1)), cv=0)

                # Fix the naming output. Works from 0 - 99
                # If this is not implemented, images are listed out of order
                extension = ""
                if steps < 10:
                    extension = '0' + str(steps)
                else:
                    extension = str(steps)

                # Create the correct file name and write it to the temporary directory
                save_file = first_image.strip('.jpg') + "_" + extension + ".jpg"
                save_file = os.path.join(workspace_dir, save_file)
                if os.path.isfile(save_file):
                    module_logger.info(save_file + " file already exist, skipping")
                else:
                    module_logger.info(save_file)
                    _image.save(save_file)

    # simple way to blend two images. Not impressive but it works ok.
    @staticmethod
    def _blend(src1, src2, alpha, step_increment, cv=0):
        img1 = Image.open(src1, 'r')
        img2 = Image.open(src2, 'r')
        alpha = alpha + step_increment
        if 0.0 <= alpha <= 1.0:
            _alpha = alpha
        else:
            _alpha = 1.0
        if cv == 0:
            return Image.blend(img1, img2, _alpha), _alpha
        elif cv != 0:
            _beta = 1.0 - _alpha
            return Image.fromarray(cv2.addWeighted(np.asarray(img1), _alpha, np.asarray(img2), _beta, 0.0)), _alpha

    def stitch(self, images, ratio=0.75, reprojThresh=4.0, showMatches=False):
        (imageB, imageA) = images
        # find the keypoints and descriptors with SIFT
        kpA, featuresA = self.detect_and_describe(imageA)
        kpB, featuresB = self.detect_and_describe(imageB)
        
        M = self.match_keypoints(kpA, kpB,
                                 featuresA, featuresB, ratio, reprojThresh)
                                 
        # if the match is None, then there aren't enough matched
        # keypoints to create a panorama
        if M is None:
            return None
        # otherwise, apply a perspective warp to stitch the images
        # together
        (matches, H, status) = M
        result = cv2.warpPerspective(imageA, H,
                                     (imageA.shape[1] + imageB.shape[1], imageA.shape[0]))
        result[0:imageB.shape[0], 0:imageB.shape[1]] = imageB

        # check to see if the keypoint matches should be visualized
        if showMatches:
            vis = self._draw_matches(imageA, imageB, kpA, kpB, matches, status)
            # return a tuple of the stitched image and the
            # visualization
            return (result, vis)

        # return the stitched image
        return result
        
    def match_keypoints(self, kpsA, kpsB, featuresA, featuresB, ratio, reprojThresh):
        # compute the raw matches and initialize the list of actual
        # matches
        # BFMatcher with default params
        min_match_count = 10
        bf = cv2.BFMatcher()
        rawMatches = bf.knnMatch(featuresA,featuresB, k=2)

        # Apply ratio test
        matches = []
        for m, n in rawMatches:
            if m.distance < 0.75*n.distance:
                matches.append(m)
       
        if len(matches) > min_match_count:
                   
            src_pts = np.float32([ kpsA[m.queryIdx].pt for m in matches ]).reshape(-1,1,2)
            dst_pts = np.float32([ kpsB[m.trainIdx].pt for m in matches ]).reshape(-1,1,2)

            H, status = cv2.findHomography(src_pts, dst_pts,  cv2.RANSAC, 4.0)
            
            return (matches,H,status)
                
        else:
            print ("Not enough matches are found - %d/%d")

    def detect_and_describe(self, image):
        # convert the image to grayscale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # check to see if we are using OpenCV 3.X
        if self.isv3:
            # detect and extract features from the image
            descriptor = cv2.xfeatures2d.SIFT_create()
            (kps, features) = descriptor.detectAndCompute(gray, None)

        # otherwise, we are using OpenCV 2.4.X
        else:
            # detect keypoints in the image
            detector = cv2.FeatureDetector_create("SIFT")
            kps = detector.detect(gray)

            # extract features from the image
            extractor = cv2.DescriptorExtractor_create("SIFT")
            (kps, features) = extractor.compute(gray, kps)

        # return a tuple of keypoints and features
        return (kps, features)

    @staticmethod
    def _draw_matches(imageA, imageB, kpsA, kpsB, matches, status):

        # Convert the list of matches into a list of lists. The function will fail otherwise
        # OpenCV 3.1.0
        converted_matches = []
        for m in matches:
            converted_matches.append([m])
        return cv2.drawMatchesKnn(imageA,kpsA,imageB,kpsB,converted_matches,None, flags=2)
        
    # Debug function to draw points for all located key points. Image is then displayed
    @staticmethod
    def _draw_keypoints(self, image, kps):
        # initialize the output visualization image
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        im = Image.fromarray(image, 'RGB')
        draw = ImageDraw.Draw(im)
        
        draw.point((kps), fill=255)
        im.show()


def main():
    ip = ImageProcessor()
    #ip.blend_folder_images(os.path.join(ip.output_folder, "forward"), 10)
    ip.generage_blended_movie_ffmgp(os.path.join(ip.output_folder, "forward"), 10)
    # img1 = cv2.imread(os.path.join(ip.output_folder, 'SE', '0001SE.jpg'))
    # img2 = cv2.imread(os.path.join(ip.output_folder, 'S', '0001S.jpg'))
    #
    # (result, vis) = ip.stitch_alternative([img1, img2], showMatches=True)
    # transformed_image = Image.fromarray(result, 'RGB')
    # transformed_image.show()
    # matching_features_image = Image.fromarray(vis, 'RGB')
    # matching_features_image.show()


if __name__ == "__main__":
    main()
